#!/usr/bin/env guile
!#

;; Copyright 2017 Todor Kondić

;; Licensed under the Apache License, Version 2.0 (the "License");
;; you may not use this file except in compliance with the License.
;; You may obtain a copy of the License at

;;     http://www.apache.org/licenses/LICENSE-2.0

;; Unless required by applicable law or agreed to in writing, software
;; distributed under the License is distributed on an "AS IS" BASIS,
;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;; See the License for the specific language governing permissions and
;; limitations under the License.


;;; Where to find schemetran modules?
(add-to-load-path "/home/todor/local/share/schemetran_site")


(use-modules (ice-9 rw))
(use-modules (ice-9 rdelim))
(use-modules (schemetran))
(use-modules (schemetran util))


;;; Changing the meaning of int and real from "integer" and "real" to
;;; "integer(8)" and "real(kind=kind(1.d0))".
(set-default-int-kind! "8")
(set-default-real-kind! "kind(1.d0)")






(define* (write-unit contents unit-fnm #:key (desc "Thing:"))
  (call-with-output-file
      unit-fnm
    (lambda (p) (write-string/partial contents p)))
  (format #t "\n* ~a written to file ~a ."
	  desc
	  unit-fnm))



;;; Define a subroutine.
(define sub-array
  (sub "array_sub"
       #:arg '("arr1" "arr2" "res1" "res2")
       #:body `(
		"!arr1 : simply contiguous real array argument with intent in"
		,(var real "arr1" #:attr `(,ini) #:ndim '(":"))
		"!arr2 : simply contiguous real array argument with intent in"
		,(var real "arr2" #:attr `(,ini) #:ndim '(":"))
		"!res1 : simply contiguous real array argument with intent in/out"
		,(var real "res1" #:attr `(,inio) #:ndim '(":"))
		"!res2 : simply contiguous real array argument with intent in/out"
		,(var real "res2" #:attr `(,inio) #:ndim '(":"))
		"res1=res1*arr1+arr2"
		"res2=res2+arr1*arr2")))


;;; A module containing the previous subroutine and two global variables.
(define the-mod
  (mod "moduledemo"
   ;; using the intrinsic modules
   #:use-i '("iso_c_binding")
   ;; define some global vars
   #:def `(,(var real "x")
	   ,(var cplx "y"))
   ;; add module procedures
   #:contains `(,sub-array)))

;;; Define the program unit depending on the 
(define the-prog
  (prog
   "progdemo"
   #:inc '("'moduledemo.f90'")
   ;; use modules
   #:use '("moduledemo, only: array_sub")
   #:body `(,(var real "a" #:attr `(,allc) #:ndim '(":"))
	    ,(var real "b" #:attr `(,allc) #:ndim '(":"))
	    ,(var real "c" #:ndim '("10"))
	    ,(var real "d" #:ndim '("10"))
	    ,(var int "i")
	    ,(var int "j")
	    "allocate(a(size(c)))"
	    "allocate(b(size(d)))"
	    "do i=1,size(c)"
	    ,(ident
	      "c(i)=i")
	    "end do"
	    "do j=1,size(d)"
	    ,(ident
	      "d(j)=j*2")
	    "end do"
	    "call array_sub(d,c,b,a)"
	    "print *, a"
	    "print *, b")))





;;; Define some variables so we can easily change the basic properties
;;; of the compilation.

;;; Compiler to use.
(define compiler "gfortran")

;;; Module directory.
(define mod-dir "mods")


;;; Produce fortran code.
(define comp-line (make-comp-line compiler
				  #:idir `(,mod-dir)
				  #:ldir '("." "..")
				  #:libs '("m")))


;;; Check for existence of the module dir. If it does not exist, make
;;; it.
(when (not (access? mod-dir F_OK))
  (mkdir mod-dir))

;;; Write the module file.
(write-unit the-mod
	    (s+ "mods/" "moduledemo.f90")
	    #:desc "Module")

;;; Write the program file.
(write-unit the-prog
	    "progdemo.f90"
	    #:desc "Program")

;;; Compile
(define the-comp-line (comp-line "progdemo.f90"
				 #:opts "-g -O0 -o progdemo"))

(newline)
(display (s+ the-comp-line "\n"))
(system the-comp-line)
