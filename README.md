<div id="table-of-contents">
<h2>Table of Contents</h2>
<div id="text-table-of-contents">
<ul>
<li><a href="#orgf049454">1. Summary</a></li>
<li><a href="#org3068b92">2. Installation</a></li>
<li><a href="#orga653749">3. How-To</a></li>
</ul>
</div>
</div>

<a id="orgf049454"></a>

# Summary

Fortran is great at expressing operations on multi-dimensional
arrays of numbers. Scheme is great at expressing some peoples coding
thoughts. This project is an attempt to combine both into something
useful.

Schemetran consists of a small number of ordinary scheme functions
and macros that can be used to generate (Scheme) strings that contain
human-readable Fortran code. The intention behind this is to have a
flexible and dynamic top-level structure of the program, written in
the awesomest computer language in existence &#x2013; Scheme &#x2013; which can be
used to quickly generate the Fortran code to do the actual (often
number-crunching) hard work.

Schemetran arose from author's own frustrations at creating and
maintaining a number of similar Fortran programs aimed at solving the
incompressible MHD equations on your run-of-the-mill supercomputing
clusters. The modern Fortran offers a decent amount of modularity via
its own modules and derived types that facilitate OOP programming,
however, the compilers, while typically great at optimising
fundamental array and matrix operations had varying degree of support
for the new idioms introduced in Fortran 2003/8 standards. After a
while, it seemed that the best approach is to delegate things Fortran
is really great at, namely mathematics, to Fortran and move all the
complexities of the design into a language in which it is easiest to
express it.

The Fortran language Schemetran produces is most of what the author
considers to be "The Good Parts" of Fortran (if you didn't by now,
please, do yourself a favour and grab Cockroft's amazing book:
"Javascript: The Good Parts"). Ancient structures of pre-90 standard
are not implemented. Likewise some of the more advanced features of
post-2003 Fortran. Abandoning old structures makes sense for obvious
reasons. Some more contemporary bits of Fortran code are still not
supported bug-free and with equal measure across the compiler
universe. Additionally, having Scheme layer on top of Fortran layer
enables expressing most of those structures and more in a better
way.


<a id="org3068b92"></a>

# Installation

Depends are guile version 2.0, or higher (preferrably >=2.2) and
modern fortran compiler (tested with Intel (>15) and GNU Fortran
(>5)).

The installation is as usual:

1.  extract the tarball
2.  enter the build directory
3.  /path/to/source/configure [potentially with &#x2013;prefix=some/non-system/installation/dir]
4.  make
5.  make install


<a id="orga653749"></a>

# How-To

The code itself is documented (check the schemetran.scm file
first). There is also a self-contained demo file under doc/examples.

